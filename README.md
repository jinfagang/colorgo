# ColorGo

## Usage

this repo using for colorful output in terminal. Just import from your project:

```
go get gitlab.com/jinfagang/colorgo
```

In your project:

```
import "gitlab.com/jinfagang/colorgo"

cg.PrintlnRed("Hello, World!")
cg.PrintlnYellow("Goodbye, World!")
```


## Advanced Usage

Not only print with color, colorgo also provide a more useful usage to highlight print certain words, like this:

```
cg.HighlightPrintln("this big word is highlighted.", "big", cg.Blue, cg.Red)
```
Then, this line will become blue and the specific word will become red!!!!

Simple and easy!! Enjoy!!!

So above code will print red and yellow in terminal.
