/*
ct package provides functions to change the color of console text.

Under windows platform, the Console API is used. Under other systems, ANSI text mode is used.
*/
package cg

import (
	"fmt"
	"strings"
)

// Color is the type of color to be set.
type Color int

const (
	// No change of color
	None = Color(iota)
	Black
	Red
	Green
	Yellow
	Blue
	Magenta
	Cyan
	White
)

var BoldStart = "\033[1m"
var BoldEnd = "\033[0m"

// ResetColor resets the foreground and background to original colors
func ResetColor() {
	resetColor()
}

// ChangeColor sets the foreground and background colors. If the value of the color is None,
// the corresponding color keeps unchanged.
// If fgBright or bgBright is set true, corresponding color use bright color. bgBright may be
// ignored in some OS environment.
func ChangeColor(fg Color, fgBright bool, bg Color, bgBright bool) {
	changeColor(fg, fgBright, bg, bgBright)
}

// Foreground changes the foreground color.
func Foreground(cl Color, bright bool) {
	ChangeColor(cl, bright, None, false)
}
// func ForegroundBold(cl Color, bright bool) {
// 	ChangeColorBold(cl, bright, None, false)
// }

// Background changes the background color.
func Background(cl Color, bright bool) {
	ChangeColor(None, false, cl, bright)
}


// Those are the color wrappers, print with different colors
func PrintlnRed(a ...interface{}){
	Foreground(Red, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnBlack(a ...interface{}){
	Foreground(Black, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnGreen(a ...interface{}){
	Foreground(Green, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnYellow(a ...interface{}){
	Foreground(Yellow, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnBlue(a ...interface{}){
	Foreground(Blue, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnMagenta(a ...interface{}){
	Foreground(Magenta, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnCyan(a ...interface{}){
	Foreground(Cyan, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}
func PrintlnWhite(a ...interface{}){
	Foreground(White, false)
	if len(a) == 1{
		fmt.Println(a[0])
	}else {
		fmt.Println(a)
	}
	ResetColor()
}


// print part
func PrintRed(a ...interface{}){
	Foreground(Red, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintBlack(a ...interface{}){
	Foreground(Black, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintGreen(a ...interface{}){
	Foreground(Green, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintYellow(a ...interface{}){
	Foreground(Yellow, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintBlue(a ...interface{}){
	Foreground(Blue, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintMagenta(a ...interface{}){
	Foreground(Magenta, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintCyan(a ...interface{}){
	Foreground(Cyan, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}
func PrintWhite(a ...interface{}){
	Foreground(White, false)
	if len(a) == 1{
		fmt.Print(a[0])
	}else {
		fmt.Print(a)
	}
	ResetColor()
}

func HighlightPrint(a string, b string, normalColor Color, highlightColor Color) {
	Foreground(normalColor, false)

	i :=  strings.Index(a, b)
	if i == -1 {
		fmt.Print(a)
	} else {
		fmt.Print(a[0: i])
		Foreground(highlightColor, false)

		if i + len(b) >= len(a) - 1 {
			fmt.Print(a[i:])
		} else {
			fmt.Print(a[i: i+len(b)])
			Foreground(normalColor, false)
			fmt.Print(a[i+len(b):])
		}
	}
	ResetColor()
}

func HighlightPrintln(a string, b string, normalColor Color, highlightColor Color) {
	Foreground(normalColor, false)

	i :=  strings.Index(a, b)
	if i == -1 {
		fmt.Println(a)
	} else {
		fmt.Print(a[0: i])
		Foreground(highlightColor, false)

		if i + len(b) >= len(a) - 1 {
			fmt.Println(a[i:])
		} else {
			fmt.Print(a[i: i+len(b)])
			Foreground(normalColor, false)
			fmt.Println(a[i+len(b):])
		}
	}
	ResetColor()
}


// This method still under test
func HighlightAllPrintln(a string, b string, normalColor Color, highlightColor Color) {
	Foreground(normalColor, false)

	idx := strings.Index(a, b)

	// this indicates the split b is the last word or not
	if idx == -1 {
		fmt.Println(a)
	} else {
		splitList := strings.Split(a, b)
		for i, s := range splitList  {
			Foreground(normalColor, false)
			fmt.Print(s)
			if i == len(splitList) - 1{
				fmt.Println("")
			} else {
				Foreground(highlightColor, false)
				fmt.Print("\033[1m")
				fmt.Print(b)
				fmt.Print("\033[0m")
			}
		}

	}
	ResetColor()
}

// This method still under test
func HighlightAllPrint(a string, b string, normalColor Color, highlightColor Color) {
	Foreground(normalColor, false)

	idx := strings.Index(a, b)

	// this indicates the split b is the last word or not
	isBInLast := a[len(a) - len(b):] == b
	if idx == -1 {
		fmt.Println(a)
	} else {
		splitList := strings.Split(a, b)
		fmt.Println("this is the split list: ", splitList)
		// if something like search.go go is the key, then the list is only 1
		if len(splitList) == 1 {
			Foreground(normalColor, false)
			fmt.Print(splitList[0])
			Foreground(highlightColor, false)
			fmt.Print(splitList[len(splitList) - 1])
		} else {
			for i, s := range splitList  {
				Foreground(normalColor, false)
				fmt.Print(s)
				if i == len(splitList) - 1{
					// to the last element, check if b in end or not
					if isBInLast {
						Foreground(highlightColor, false)
						fmt.Print(b)
					} else {
					}
				} else {
					Foreground(highlightColor, false)
					fmt.Print("\033[1m")
					fmt.Print(b)
					fmt.Print("\033[0m")
				}
			}
		}
	}
	ResetColor()
}